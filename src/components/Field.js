import React from 'react';
import LanguageContext from './../contexts/LanguageContext'

const Field = () => {
  const renderLabel = (language) => {
    const text = language === 'english' ? 'Name' : 'Naam'
    return <label>{ text }</label>
  }

  return (
    <div className="ui field">
      <LanguageContext.Consumer>
        { ({ language }) => renderLabel(language) }
      </LanguageContext.Consumer>
      <input />
    </div>
  )
}

export default Field;